
### Nombre: Alejandro Lafuente Cacho
### Asignatura: Analisis de aplicaciones empresariales
### Fecha: 05/12/2019
### Descripcion: Script para unir varios .csv en un unico archivo

# Para borrar el archivo .DS_Store en sistemas MAC_OS
# find . -name ".DS_Store" -delete

#################
### Librerias ###
#################

import os, sys


#########################
### Ajustes Iniciales ###
#########################

#path = "/Users/Alejandro/Desktop/Salida/"
path = "C:/Users/alexl/Desktop/proyectofinal_aaee/Transformados/"
archivos = os.listdir(path)


##########################
### Programa Principal ###
##########################

for x in archivos:
    # En go guardamos la ruta de cada carpeta de cada una de las estaciones
    #go = "/Users/Alejandro/Desktop/Salida/"+x+"/"
    go = "C:/Users/alexl/Desktop/proyectofinal_aaee/Transformados/"+x+"/"
    ficheros = os.listdir(go)
    # fout es nuestro archivo de salida en el que saldran los .csv agrupados
    #fout = open("/Users/Alejandro/Desktop/SalidaUnica/"+x+".csv","a")
    fout = open("C:/Users/alexl/Desktop/proyectofinal_aaee/SalidaUnica/"+x+".csv","a")
    # fichCabecera es el fichero en el cual obtendremos la cabecera de los datos,
    # es decir, temp media, temp max, temp min y precipt media
    #fichCabecera = open("/Users/Alejandro/Desktop/Salida/"+x+"/"+x+"_2011.csv")
    fichCabecera = open("C:/Users/alexl/Desktop/proyectofinal_aaee/Transformados/"+x+"/"+ficheros[0])
    for line in fichCabecera:
        fout.write(line)
        break
    for i in ficheros:
        # fichero es cada uno de los ficheros .csv de cada estacion, de los cuales
        # cogeremos todos los datos excepto su cabecera
        #fichero = open("/Users/Alejandro/Desktop/Salida/"+x+"/"+i)
        fichero = open("C:/Users/alexl/Desktop/proyectofinal_aaee/Transformados/"+x+"/"+i)
        # next() sirve para saltarnos la cabecera de cada archivo
        fichero.next()
        for line in fichero:
            fout.write(line)
        fichero.close()
    fout.close()

print ("//////////////////")
print ("//////////////////")
print ("Proceso Completado")
print ("//////////////////")
print ("//////////////////")
