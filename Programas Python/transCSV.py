
### Nombre: Alejandro Lafuente Cacho
### Asignatura: Analisis de aplicaciones empresariales
### Fecha: 10/12/2019
### Descripcion: Script para anadir la columna localidad a los csv, asi como sustituir valores
### nulos por 0.0

#################
### Librerias ###
#################

import os
import csv


#########################
### Ajustes Iniciales ###
#########################

#path = "/home/alumno/Escritorio/proyectofinal_aaee/SalidaUnica/"
path = "C:/Users/alexl/Desktop/proyectofinal_aaee/Salida/"
archivos = os.listdir(path)


##########################
### Programa Principal ###
##########################

for x in archivos:
	#csvfile = "/home/alumno/Escritorio/proyectofinal_aaee/SalidaUnica/"+x
	regiones = "C:/Users/alexl/Desktop/proyectofinal_aaee/Salida/"+x+"/"
	region = os.listdir(regiones)
	for csvf in region:
		csvfile = "C:/Users/alexl/Desktop/proyectofinal_aaee/Salida/"+x+"/"+csvf
		#csvout = "/home/alumno/Escritorio/proyectofinal_aaee/Transformados/"+x
		csvout = "C:/Users/alexl/Desktop/proyectofinal_aaee/Transformados/"+x+"/"+csvf
		with open(csvfile, "rb") as fin, open(csvout, "wb") as fout: 
			reader = csv.reader(fin, lineterminator='\n') 
			writer = csv.writer(fout, lineterminator='\n') 
			# Se anade a la cabecera una columna llamada "Localidad"
			writer.writerow(["Comarca"] + next(reader) + ["Localidad"])
			# Anadimos a cada fila una columna con la localidad que le corresponde 
			for line in reader:
				for n, i in enumerate(line):
					if i == "-":
						line[n] = "0.0"
				writer.writerow([x] + line + [csvf[:-9]])

print ("//////////////////")
print ("//////////////////")
print ("Proceso Completado")
print ("//////////////////")
print ("//////////////////")
