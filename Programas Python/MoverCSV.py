
### Nombre: Alejandro Lafuente Cacho
### Asignatura: Analisis de aplicaciones empresariales
### Fecha: 05/12/2019
### Descripcion: Script para adecuar los datos .csv para su subida a BBDD


#################
### Librerias ###
#################

import os, sys
import shutil


#########################
### Ajustes Iniciales ###
#########################

# Indicamos la ruta donde se encuentran los datos de csv
# path = "/Users/Alejandro/Desktop/res/"
path = "C:/Users/alexl/Desktop/proyectofinal_aaee/res/"
res = os.listdir(path)

# Lista en la que guardamos los IDs
vectorID = []

# Diccionario en el que guardaremos los IDs junto a sus Nombres
dic = dict()


#####################################################################
### Tratamiento del .txt para obtener los datos en un diccionario ###
#####################################################################

# fname = "/Users/Alejandro/Desktop/ids_estaciones.txt"
fname = "C:/Users/alexl/Desktop/proyectofinal_aaee/ids_estaciones.txt"
etiquetas = open(fname)

# En primer lugar separamos los string en dos partes, el ID y el nombre
for x in etiquetas:
	xsep = x.split("*")
	#print(xsep[1])
	#print(xsep[2][:-1])
	# Asignamos creando un diccionario
	dic[xsep[0]] = [xsep[1],xsep[2][:-1]]
#print(dic)


################################
### Guardar IDs en una lista ###
################################

for x in res:
	if (x[:-9]) not in vectorID:
		vectorID.append(x[:-9])


#####################################################
### Crear carpetas respecto a los IDs de la lista ###
#####################################################

for ID in vectorID:
	# outdir = "/Users/Alejandro/Desktop/Salida/"+dic[ID]
	#outdir = "C:/Users/alexl/Desktop/proyectofinal_aaee/Salida/"+dic[ID][1]+"/"+dic[ID][0]
	outdir = "C:/Users/alexl/Desktop/proyectofinal_aaee/Salida/"+dic[ID][1]
	# En caso de tener las carpetas ya creadas nos dara error si las intentamos
	# crear de nuevo. Deberemos borrarlas todas y ejecutar de nuevo el script.
	try:
		os.mkdir(outdir)
	except OSError:
		pass
	
	
	
##########################################################
### Metemos cada .csv en la carpeta que le corresponde ###
##########################################################


# Bucle para meter cada .csv en su correspondiente carpeta
for csv in res:

	#direntr = "/Users/Alejandro/Desktop/res/"+csv
	direntr = "C:/Users/alexl/Desktop/proyectofinal_aaee/res/"+csv
	#carpeta = "/Users/Alejandro/Desktop/Salida/"+dic[csv[:-9]]+"/"+csv
	#carpeta = "C:/Users/alexl/Desktop/proyectofinal_aaee/Salida/"+dic[csv[:-9]][1]+"/"+dic[csv[:-9]][0]+"/"+csv
	carpeta = "C:/Users/alexl/Desktop/proyectofinal_aaee/Salida/"+dic[csv[:-9]][1]+"/"+csv

	shutil.copyfile(direntr, carpeta)

	# Cambiamos de nombre al fichero, poniendole el nombre en vez del ID
	# dirout = "/Users/Alejandro/Desktop/Salida/"+(dic[csv[:-9]])+"/"+(dic[csv[:-9]])+(csv[-9:])
	print(dic[csv[:-9]][0])
	#dirout = "C:/Users/alexl/Desktop/proyectofinal_aaee/Salida/"+dic[csv[:-9]][1]+"/"+dic[csv[:-9]][0]+"/"+(dic[csv[:-9]][0])+(csv[-9:])
	dirout = "C:/Users/alexl/Desktop/proyectofinal_aaee/Salida/"+dic[csv[:-9]][1]+"/"+(dic[csv[:-9]][0])+(csv[-9:])
	os.rename(carpeta,dirout)


print ("//////////////////")
print ("//////////////////")
print ("Proceso Completado")
print ("//////////////////")
print ("//////////////////")